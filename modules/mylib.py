from igraph import *


def render(filename,**variables):
    context = globals()
    context.update(variables)
    from gluon.template import render
    return render(filename=os.path.join(request.folder,'views',filename),
                  path=os.path.join(request.folder,'views'),
                  context=context,delimiters=('{%','%}')) 

# --------------------------------------------------------- #

# Reads a file as a table (quite similar to R)
def read_table(path, sep):
	f = open(path)
	t = []
	for row in f.readlines():
		t.append(row.strip().split(sep))
	f.close()
	return t

# Converts a string into a table
def make_table(s, rowsep, colsep):
	t = []
	for row in s.split(rowsep):
		t.append(row.split(colsep))
	return t

def make_float_table(s, rowsep, colsep):
	t = []
	for row in s.split(rowsep):
		tmp = []
		for e in row.split(colsep):
			tmp.append(float(e))
		t.append(tmp)
	return t

# Retrieves a column from a matrix (index starts from 0)
def get_column(table, column):
	col = []
	for row in table:
		col.append(row[column])
	return col

# Uniqifies a list
# IMPORTANT: does not preserve order
def uniq(l):
	return list(set(l))

# Adds a suffix to every element of a strings list
def suffixList(l, suf):
	l2 = []
	for e in l:
		l2.append(e + suf)
	return l2

# Converts a vertex name into the vertex id
def vnameTOvid(g, vn):
	return(g.vs['name'].index(vn))

# Converts a list of vertex name into list of vertex id
def vnamelTOvidl(g, lvn):
	nodes = uniq(g.vs['name'])
	dnid = {}
	for i in range(len(nodes)):
		dnid[nodes[i]] = vnameTOvid(g,nodes[i])
	l = []
	for vn in lvn:
		l.append(dnid[vn])
	return l

# Converts two extremities lists into a list of extremity couples
def edgesFromExtremities(lsources, ltargets):
	if len(lsources) != len(ltargets):
		return(False)
	l = []
	for i in range(len(lsources)):
		l.append((lsources[i], ltargets[i]))
	return l

# Replicates each elements of a list a certain number of times (R)
def repl(el, times):
	if len(el) != len(times):
		return False
	l = []
	for i in range(len(el)):
		e = el[i]
		t = times[i]
		c = 0
		while c < t:
			l.append(e)
			c += 1
	return(l)

# Prepares histogram
def prepHist(ld, breaks):
	top = float(max(ld))
	bot = float(min(ld))
	d = {}
	# Calc bins & mids
	d['mid'] = []
	d['bin'] = []
	d['count'] = []
	width = (top - bot) / breaks
	for i in range(breaks):
		binbot = bot + width * i
		bintop = bot + width * (i + 1)
		d['bin'].append( (binbot,bintop) )
		d['mid'].append( binbot + width / 2 )
		d['count'].append(0)
	# Calc counts
	ld.sort()
	i = 0
	dbin = d['bin'][i]
	for data in ld:
		done = False
		while not done:
			if data >= dbin[0] and data < dbin[1]:
				d['count'][i] += 1
				done = True
			elif dbin[1] == top and data == dbin[1]:
				d['count'][i] += 1
				done = True
			else:
				i += 1
				dbin = d['bin'][i]
	# Calc Densities
	d['density'] = []
	for i in range(len(d['count'])):
		d['density'].append(d['count'][i] / float(len(ld)))
	return d

def which_gt(l, thr):
	li = []
	for i in range(len(l)):
		if l[i] > thr:
			li.append(i)
	return li

def which_lt(l, thr):
	li = []
	for i in range(len(l)):
		if l[i] < thr:
			li.append(i)
	return li

def which_eq(l, thr):
	li = []
	for i in range(len(l)):
		if l[i] == thr:
			li.append(i)
	return li

def which_ge(l, thr):
	li = []
	for i in range(len(l)):
		if l[i] >= thr:
			li.append(i)
	return li

def which_le(l, thr):
	li = []
	for i in range(len(l)):
		if l[i] <= thr:
			li.append(i)
	return li

def attr_filter_vs(v, show_attr, hide_attr, attr):
	if len(show_attr) != 0:
		r = False
		for val in show_attr:
			if val in v[attr]:
				r = True
		if not r:
			return r
	if len(hide_attr) != 0:
		for val in hide_attr:
			if val in v[attr]:
				return False
	return True

def string2float(s):
	s = s.split('.')
	if len(s) == 1:
		if s[0].isdigit():
			s = float(s[0])
		else:
			s = None
	elif len(s) == 2:
		if s[0].isdigit() and s[1].isdigit():
			s = float('.'.join(s))
		else:
			s = None
	else:
		s = None
	return s
