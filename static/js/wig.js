(function () {
    var app = angular.module('wig-index', []);

    /* CONTROLLERS */

    app
        .controller('indexController', function () {
            this.pageIndex = 0;
            this.setIndex = function (v) {
                this.pageIndex = v;
            };
            this.isIndex = function (v) {
                return this.pageIndex === v;
            };
        });

    /* DIRECTIVES */

    app
        .directive('welcomePage', function () {
            return {
                restrict: 'E',
                templateUrl: '../static/dirs/welcome-page.html',
                controller: 'indexController',
                controllerAs: 'iCtrl'
            };
        })
        .directive('mainMenu', function () {
            return {
                restrict: 'E',
                templateUrl: '../static/dirs/main-menu.html',
                controller: 'indexController',
                controllerAs: 'iCtrl'
            };
        })
        .directive('uploadNetworkForm', function () {
            return {
                restrict: 'E',
                templateUrl: '../static/dirs/upload-network-form.html',
                controller: function () {

                }
            };
        })
        .directive('selectNetworkList', function () {
            return {
                restrict: 'E',
                templateUrl: '../static/dirs/select-network-list.html',
                controller: function () {

                }
            };
        })
        .directive('settingsForm', function () {
            return {
                restrict: 'E',
                templateUrl: '../static/dirs/settings-form.html',
                controller: function () {

                }
            };
        });

}());